## Project Documentation

# WanderWise

- Gianni Thi
- Ikran Haji
- Jason Lago
- Jeny Kim
- Paul Baumann

WanderWise - Welcome to WonderWise, the app that revolutionizes your travel experience. Powered by AI, we provide personalized travel suggestions, prioritize your safety, offer culinary delights, recommend hobbies, and unlock global adventures.

## Design

- [API design](APIdesign.MD)
- [Data model](Datamodel.MD)
- [GHI](GHI.MD)
- [Intergrations](Integrations.MD)

## Functionality

- Visitors to our website can create a profile
- Once a profile is created users can input interests and a location,
  and recieve tailored recommendations based on those prerequisites.
- Recommendations can be added/ saved to their user profiles to look at, at a later date or deleted.
- On a users profile they can click on each individual recommendation to view the
  details and delete there as well.
